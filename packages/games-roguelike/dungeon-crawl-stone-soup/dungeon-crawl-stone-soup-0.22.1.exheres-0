# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Copyright 2016 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require lua [ multibuild=false whitelist="5.1" ] gtk-icon-cache game

MY_PN=stone_soup
MY_PNV=${MY_PN}-${PV}

SUMMARY="Single-player, role-playing roguelike game"
HOMEPAGE="https://crawl.develz.org/wordpress/"
DOWNLOADS="https://crawl.develz.org/release/$(ever range 1-2)/${MY_PNV}-nodeps.tar.xz"

LICENCES="
    GPL-2
    BSD-3 [[ note = [ mt19937ar.cc, MSVC/stdint.h ] ]]
    BSD-2 [[ note = [ all contributions by Steve Noonan and Jesse Luehrs ] ]]
    public-domain [[ note = [ most of tiles ] ]]
    CC0 [[ note = [ most of tiles ] ]]
    MIT [[ note = [ json.cc/json.h, some .js files in
                    webserver/static/scripts/contrib/ ] ]]
"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    tiles [[ description = [ Do graphical (tiled) build instead of ncurses build ] ]]
"

# lots of sydbox access violations
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        sys-devel/bison
        sys-devel/flex
        virtual/pkg-config
        tiles? ( sys-libs/ncurses )
    build+run:
        dev-db/sqlite:3
        sys-libs/zlib
        !tiles? ( sys-libs/ncurses )
        tiles? (
            fonts/dejavu
            media-libs/freetype:2
            media-libs/libpng:=
            media-libs/SDL:2[X]
            media-libs/SDL_image:2
            x11-dri/glu
            x11-dri/mesa
        )
"

WORK=${WORKBASE}/${MY_PNV}/source

MY_SAVEDIR=/var/lib/games/${MY_PN}

DEFAULT_SRC_COMPILE_PARAMS=(
    DATADIR="/usr/share/${PN}"
    SAVEDIR=${MY_SAVEDIR}
    DESTDIR="${IMAGE}"
    bin_prefix="${IMAGE}/usr/$(exhost --target)/bin"
    prefix_fp=""
    prefix="/usr"

    USE_LUAJIT=
    USE_UNICODE=y
    BUILD_ALL=
    NO_TRY_GOLD=y
    V=1

    INSTALL_UGRP=wizard:games
    MCHMOD=ug+s

    EXTERNAL_FLAGS="${CXXFLAGS}"
    EXTERNAL_LDFLAGS="${LDFLAGS}"

    HOST="$(exhost --target)"

    AR="${AR}"
    RANLIB="${RANLIB}"
    CC="${CC}"
    CXX="${CXX}"
    PKGCONFIG="${PKG_CONFIG}"
    STRIP=echo
)

src_prepare() {
    default

    # fix desktop files
    edo sed \
        -e 's:/usr/games/crawl:/usr/bin/crawl:g' \
        -i debian/crawl.desktop
    edo sed \
        -e 's:/usr/games/crawl-tiles:/usr/bin/crawl:g' \
        -i debian/crawl-tiles.desktop
}

src_compile() {
    export HOSTCXX=${CXX} GXX=${CXX}

    emake "${DEFAULT_SRC_COMPILE_PARAMS[@]}" \
        $(option tiles "TILES=y" "")
}

src_install() {
    emake "${DEFAULT_SRC_COMPILE_PARAMS[@]}" \
        $(option tiles "TILES=y" "") \
        install

    keepdir ${MY_SAVEDIR}/morgue
    keepdir ${MY_SAVEDIR}/saves/db
    preserve_scores "${IMAGE}"${MY_SAVEDIR}

    dovarlibgames -R

    # install desktop file
    insinto /usr/share/applications
    if option tiles; then
        doins debian/crawl-tiles.desktop
    else
        doins debian/crawl.desktop
    fi

    # install icon
    insinto /usr/share/pixmaps
    doins debian/crawl.xpm
}

pkg_preinst() {
    game_pkg_preinst
    gtk-icon-cache_pkg_preinst
}

pkg_postinst() {
    game_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

