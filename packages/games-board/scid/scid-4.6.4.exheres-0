# Copyright 2014 Ali Polatel <alip@exherbo.org>
# Based in part upon scid-4.5.2.ebuild of Gentoo, which is:
#   Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

require game sourceforge [ suffix=zip ]

SUMMARY="Shane's Chess Information Database"
DOWNLOADS+="
    mirror://sourceforge/scid/spelling.zip
    mirror://sourceforge/scid/ratings.zip
    mirror://sourceforge/scid/photos.zip
    mirror://sourceforge/scid/scidlet40k.zip
"

BUGS_TO="alip@exherbo.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# TODO:
#   - Scid can make use of snack.
#   - Some scripts need python.
DEPENDENCIES="
    build+run:
        dev-lang/tk
        dev-tcl/tkimg
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-4.6.4-build-fixes.patch
)

src_prepare() {
    default

    edo sed \
        -e "s:@GAMES_DATADIR@:/var/lib/games/${PN}:" \
        -i tcl/{options,start}.tcl

    edo gzip ../ratings.ssp
}

src_configure() {
    # configure is not an autotools script
    edo ./configure \
        CC="${CC}" \
        COMPILE="${CXX}" \
        LINK="${CXX} ${CXXFLAGS} ${LDFLAGS}" \
        OPTIMIZE="${CXXFLAGS}" \
        TCL_INCLUDE='' \
        BINDIR="/usr/$(exhost --target)/bin" \
        SHAREDIR="/var/lib/games/${PN}"
}

src_compile() {
    emake all_scid
}

src_install() {
    emake DESTDIR="${IMAGE}" install_scid
    emagicdocs

    # install icon
    insinto /usr/share/icons/hicolor/scalable/apps
    newins svg/scid_app.svg ${PN}.svg

    insinto /var/lib/games/${PN}
    doins -r sounds
    edo cd ..
    doins spelling.ssp ratings.ssp.gz *.spf
    newins scidlet40k.sbk scidlet.sbk

    keepdir /var/lib/games/${PN}/bases

    # install desktop file
    insinto /usr/share/applications
    hereins ${PN,,}.desktop <<EOF
[Desktop Entry]
Name=Scid
Version=1.1
Exec=${PN}
Comment=A Free Chess Database Application
Icon=${PN}.svg
Type=Application
Terminal=false
StartupNotify=true
Categories=Game;BoardGame;
EOF
}

