# Copyright 2009, 2010 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require cmake [ api=2 ]

SUMMARY="A free cross-platform lobby client for the Spring RTS project"
HOMEPAGE="https://springlobby.info"
DOWNLOADS="${HOMEPAGE}/tarballs/${PNV}.tar.bz2"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    notifications [[ description = [ Enables libnotify support for popup status messages ] ]]
    sound [[ description = [ Enable sound support ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        sys-devel/gettext
    build+run:
        dev-libs/boost[>=1.42.0]
        net-misc/curl
        x11-libs/libX11
        x11-libs/wxGTK:3.0[>=2.9]
        notifications? (
            dev-libs/glib:2
            x11-libs/libnotify
        )
        sound? (
            media-libs/alure
            media-libs/openal
        )
    suggestion:
        games-strategy/spring
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-CMakeLists.txt-Use-GNUInstallDirs.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
    -DOPTION_TRANSLATION_SUPPORT:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'notifications OPTION_NOTIFY'
    'sound OPTION_SOUND'
)

