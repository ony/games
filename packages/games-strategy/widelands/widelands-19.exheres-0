# Copyright 2010, 2011 Johannes Nixdorf <mixi@user-helfen-usern.de>
# Distributed under the terms of the GNU General Public License v2

MY_PV=build${PV}

require launchpad [ branch=${MY_PV} pv=${MY_PV} pnv=${PN}-${MY_PV}-src suffix=tar.bz2 ] \
    cmake [ api=2 ] \
    freedesktop-desktop \
    gtk-icon-cache

SUMMARY="Widelands is an real-time strategy game similar to the Settlers II"
HOMEPAGE="https://wl.widelands.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# TODO: bundled fonts
DEPENDENCIES="
    build:
        dev-lang/python:*
        sys-devel/gettext
    build+run:
        dev-libs/boost[>=1.48.0]
        dev-libs/icu:=
        media-libs/SDL:2
        media-libs/SDL_image:2
        media-libs/SDL_mixer:2[ogg]
        media-libs/SDL_net:2
        media-libs/SDL_ttf:2
        media-libs/glew
        media-libs/libpng:=
        sys-libs/zlib
        x11-dri/mesa [[ note = [ provides libGL ] ]]
"

# -DCMAKE_BUILD_TYPE: the files are only installed for Release and Debug
CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_DOCUMENTATION:BOOL=FALSE
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DOPTION_BUILD_TRANSLATIONS:BOOL=TRUE
    -DOPTION_BUILD_WEBSITE_TOOLS:BOOL=FALSE
    -DOPTION_USE_GLBINDING:BOOL=FALSE
    -DWL_INSTALL_BASEDIR:PATH=/usr/share/games/${PN}
    -DWL_INSTALL_DATADIR:PATH=/usr/share/games/${PN}
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

CMAKE_SOURCE=${WORKBASE}/${PN}-${MY_PV}-src

src_prepare() {
    cmake_src_prepare

    # -DCMAKE_CXX_FLAGS_RELEASE above doesn't work due to CMakeLists hardcoding.
    # disable -Werror
    edo sed \
        -e '/WL_OPTIMIZE_FLAGS/d' \
        -e '/-Werror/d' \
        -i CMakeLists.txt

    edo sed \
        -e 's:/usr/share/games/widelands/data/images/logos/wl-ico-64.png:widelands.png:g' \
        -i debian/widelands.desktop
}

src_install() {
    cmake_src_install

    # move the game binary to bin
    dodir /usr/$(exhost --target)/bin
    edo mv "${IMAGE}"/usr/$(exhost --target){,/bin}/${PN}

    # remove wl_render_richtext which is not indended for the user
    edo rm "${IMAGE}"/usr/$(exhost --target)/wl_render_richtext

    # install desktop file
    insinto /usr/share/applications
    doins "${CMAKE_SOURCE}"/debian/${PN}.desktop

    # install icons
    for size in 16 32 48 64 128 ; do
        insinto /usr/share/icons/hicolor/${size}x${size}/apps
        newins "${CMAKE_SOURCE}"/data/images/logos/wl-ico-${size}.png ${PN}.png
    done

    # remove empty directories
    edo find "${IMAGE}" -type d -empty -delete
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

